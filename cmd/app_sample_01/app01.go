package main

import (
	"local_single_module/src/lib_sample_01"
	"local_single_module/src/lib_sample_02"
)

func main() {
	println("I am sample application 1")

	lib_sample_01.Lib1Hello()

	lib_sample_02.Lib2Hello()
}
